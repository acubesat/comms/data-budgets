"""Assign the values of the GMAT files to a np object."""
import numpy as np


def AssignValues(x, f, b, c):
    """Assignvalues takes values from GMAT.txt and assigns them to a vector.

     The elements of the vector that we care about are the first (day that
     the satelite becomes visible) and the last(seconds that we can see the
     satelite).
     :param x: x is used for assigning the values from the file to it
     :type x: numpy.array object
     :param f: The file from which we will take the wanted values.
     :type f: file object
     :param n: is the number of lines of the file
     :type n: integer
    :returns: x
    :rtype: numpy.array object
    """
    i = 0
    for lines in f:
        i = i+1
        # Spliting the line that we read
        a = lines.split(' ')
        if(i >= b and i <= c):
            a[len(a)-1] = a[len(a)-1].rstrip('\n')
            x = np.insert(x, i-b, [((a[0]), float(a[len(a)-1]))])
    return x
