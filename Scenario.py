"""Definition of Scenario Function."""

import numpy as np
import datetime


def Scenario(data, b, c):
    """Calculate the average comms window and the total contact time.

    It takes the data of the GMAT file and the n, number of lines of the
    file and calculates the average comms window of each pass of the
    satelite and the total contact time of the satelite. It also prints
    those results.
    :param data: It contains the data of the day and total time that the
    satelite is above the ground station
    :type data: np.array
    :param n: Total lines of the file
    :type n:integer
    :returns:Pass_Communication_Window, Mission_Duration_Seconds
    :rtype:float
    """
    Pass_Comms_Window = np.mean(data["Duration"])
    Mission_Duration_Sec = np.sum(data['Duration'], 0)
    print("Mean time of pass :" + str(Pass_Comms_Window)+"seconds")
    print("Total Duration:"+str(Mission_Duration_Sec)+"seconds")
    print("Total duration:"+str(datetime.timedelta(seconds=float(Mission_Duration_Sec)))+"hours")
    j = 0
    for i in range(1, (c-b)):
        if(data[i]['Start_day'] != data[i-1]["Start_day"]):
            j = j+1
    Total_Days = j
    print("Total days:"+str(j))
    print("Total months:"+str(j/30))
    Av_passes = (c-b)/j
    print("Average passes per days :"+str(Av_passes)+"\n")
    return Pass_Comms_Window, Mission_Duration_Sec, Av_passes, Total_Days
