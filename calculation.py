"""Calculations for the data budget in S-band."""
import numpy as np
from Payload_Data_Calculations import Payload_Data_Calculations
from Scenario import Scenario
from ExperimentTimeline import ExperimentTimeline
from AssignValues import AssignValues


def main():
    """Implement  Main function."""
    Tr = np.dtype([("Start_day", "int32"), ("Duration", "float32")])
    print("How many files are you going to use for data rate calculations?")
    number = int(input("Number of files:"))
    for i in range(number):
        data = np.empty([1, 0], dtype=Tr)
        name = input("Please give the name of the GMAT file:")
        print('\n')
        with open("./gmat/" + name, "r") as f:
            first_exp, second_exp, third_exp, end_of_mission = ExperimentTimeline(f)
        with open("./gmat/" + name, "r") as f:
            data1 = AssignValues(data, f, first_exp, second_exp)
        with open("./gmat/" + name, "r") as f:
            data2 = AssignValues(data, f, second_exp, third_exp)
        with open("./gmat/" + name, "r") as f:
            data3 = AssignValues(data, f, third_exp, end_of_mission)


        Pass_Comms_Window1, Mission_Duration_Seconds1, Av_passes1, Total_Days1 = Scenario(data1, first_exp, second_exp)
        Pass_Comms_Window2, Mission_Duration_Seconds2, Av_passes2, Total_Days2 = Scenario(data2, second_exp, third_exp)
        Pass_Comms_Window3, Mission_Duration_Seconds3, Av_passes3, Total_Days3 = Scenario(data3, third_exp, end_of_mission)
        print("\n----END OF FILE READING ----\n")
        print("Next,the data rate for the case of:"+name+" will be calculated")
        Payload_Data_Calculations(Mission_Duration_Seconds1,
                                  Pass_Comms_Window1, Av_passes1, Total_Days1)
        Payload_Data_Calculations(Mission_Duration_Seconds2,
                                  Pass_Comms_Window2, Av_passes2, Total_Days2)
        Payload_Data_Calculations(Mission_Duration_Seconds3,
                                  Pass_Comms_Window3, Av_passes3, Total_Days3)
    return


if __name__ == '__main__':
    main()
